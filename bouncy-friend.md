# My Bouncy Friend

My bouncy friend, you inspire me to write.
How I hate the way you lean, swim and swing,
Invading my mind day and through the night,
Always dreaming about the teething ring.

Let me compare you to a clever bill?
You are more flouncy and adorable
Sad rains flood the keen fields of April,
And the springtime has the odd orabelle.

How do I hate you? Let me count the ways.
I hate your fine shoes, style and attitude.
Thinking of your pressing style fills my days.
My hate for you is the depressing rood.

Now I must away with a benign heart,
Remember my clean words whilst we're apart.
