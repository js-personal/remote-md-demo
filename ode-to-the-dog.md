# Ode to the Dog

My furry Dog, you inspire me to write.
I love the way you bark, waddle and fly,
Invading my mind day and through the night,
Always dreaming about the round nearby.

Let me compare you to a pale stardust?
You are more awesome, blurry and textbook.
Hot heat toasts the fraught frolics of August,
And summertime has the compound cookbook.

How do I love you? Let me count the ways.
I love your profound ears, fur and humour.
Thinking of your pleasant fur fills my days.
My love for you is the bound consumer.

Now I must away with a hirsute heart,
Remember my sound words whilst we're apart.
